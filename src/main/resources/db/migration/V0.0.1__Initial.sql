CREATE TABLE products
(
    sku         VARCHAR(16)     NOT NULL
        CONSTRAINT pk_product_id PRIMARY KEY,
    name        VARCHAR(125)    NOT NULL,
    description VARCHAR(125),
    price       DECIMAL           NOT NULL,
    created_at  TIMESTAMP         NOT NULL,
    updated_at  TIMESTAMP         NOT NULL
);

ALTER TABLE products
ADD COLUMN stock_level INT NOT NULL AFTER price;
