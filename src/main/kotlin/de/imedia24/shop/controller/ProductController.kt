package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import de.imedia24.shop.utils.ResponseCodes
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @Operation(summary = "Get product's details", description = "Returns 200 if successful")
    @ApiResponses(
            value = [
                ApiResponse(responseCode = ResponseCodes.OK, description = "Successful Operation"),
                ApiResponse(responseCode = ResponseCodes.NOT_FOUND, description = "A product may not exists"),
                ApiResponse(responseCode = ResponseCodes.SERVER_ERROR, description = "Internal server error"),
            ]
    )
    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @Operation(summary = "Get a list of products using skus", description = "Returns 200 if successful")
    @ApiResponses(
            value = [
                ApiResponse(responseCode = ResponseCodes.OK, description = "Successful Operation"),
                ApiResponse(responseCode = ResponseCodes.NOT_FOUND, description = "A product may not exists"),
                ApiResponse(responseCode = ResponseCodes.SERVER_ERROR, description = "Internal server error"),
            ]
    )
    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
            @RequestParam("skus") skus: List<String>
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for list products $skus")

        val products = productService.findProductsBySkus(skus)
        return if(products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @Operation(summary = "Add new product", description = "Returns 201 if successful")
    @ApiResponses(
            value = [
                ApiResponse(responseCode = ResponseCodes.CREATED, description = "Product created"),
                ApiResponse(responseCode = ResponseCodes.NOT_FOUND, description = "A product may not exists"),
                ApiResponse(responseCode =ResponseCodes.SERVER_ERROR, description = "Internal server error"),
            ]
    )
    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun addProduct(
            @RequestBody product: ProductRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to add a new product: $product")

        val product = productService.addProduct(product)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @Operation(summary = "Update existing product", description = "Returns 201 if successful")
    @ApiResponses(
            value = [
                ApiResponse(responseCode = ResponseCodes.CREATED, description = "Product updated"),
                ApiResponse(responseCode = ResponseCodes.NOT_FOUND, description = "A product may not exists"),
                ApiResponse(responseCode = ResponseCodes.SERVER_ERROR, description = "Internal server error"),
            ]
    )
    @PutMapping("/products", produces = ["application/json;charset=utf-8"])
    fun updatePartialProduct(
            @RequestBody product: ProductRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request update partial product $product")

        var existingProduct = productService.findProductBySku(product.sku)
        if (existingProduct == null) {
            return ResponseEntity.notFound().build()
        }
        return ResponseEntity.ok(productService.updateProduct(product));
    }

}
