package de.imedia24.shop.db.entity

import io.swagger.v3.oas.annotations.media.Schema
import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "products")
@Schema(description = "Product model.")
data class ProductEntity
(
    @Id
    @Column(name = "sku", nullable = false)
    val sku: String,

    @Column(name = "name", nullable = false)
    val name: String,

    @Column(name = "description")
    val description: String? = null,

    @Column(name = "price", nullable = false)
    val price: BigDecimal,

    @Column(name = "stock_level", nullable = false)
    val stockLevel: Int,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = false)
    val createdAt: ZonedDateTime,

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    val updatedAt: ZonedDateTime
){
     constructor() :this(" "," ", "",BigDecimal.ZERO, 0,ZonedDateTime.now(),ZonedDateTime.now())
}


