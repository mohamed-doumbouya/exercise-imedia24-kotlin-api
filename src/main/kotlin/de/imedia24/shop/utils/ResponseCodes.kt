package de.imedia24.shop.utils

object ResponseCodes {
    const val OK = "200";
    const val CREATED = "201";
    const val NOT_FOUND = "404";
    const val SERVER_ERROR = "500";
}