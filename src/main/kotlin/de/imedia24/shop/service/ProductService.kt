package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse();
    }

    fun findProductsBySkus(skus: List<String>): List<ProductResponse>? {
        return productRepository.findBySkuIn(skus)?.map { p ->p.toProductResponse() };
    }

    fun addProduct(product: ProductRequest): ProductResponse? {
        if (product == null || product.sku == null) {
            throw  IllegalArgumentException("Product or ID must not be null!");
        }
        return productRepository.save(product.toProductEntity())?.toProductResponse();
    }

    fun updateProduct(product: ProductRequest): ProductResponse {
        val updatedProduct:ProductEntity= product.toProductEntity().copy(updatedAt = ZonedDateTime.now())
        return productRepository.save(updatedProduct)?.toProductResponse();
    }

}
