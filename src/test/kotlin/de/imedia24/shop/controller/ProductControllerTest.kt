package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.Test
import org.mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal
import java.util.*

@WebMvcTest(ProductController::class)
class ProductControllerTest {

    @Autowired
    private lateinit var mvc:MockMvc

    @MockBean
    lateinit var productService: ProductService

    @Test
    fun findProductsBySkus_success(){
        val skus = Arrays.asList("123", "8574", "8901", "2345", "67789")
        Mockito.`when`(productService.findProductsBySkus(skus)).thenReturn(buildProductList())

         mvc.perform(MockMvcRequestBuilders
                .get("/products")
                .queryParam("skus", skus.joinToString(","))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                 .andExpect(jsonPath("$").isNotEmpty)
                 .andExpect(jsonPath("$[0].name").value("Xiamo"))

    }

    fun buildProductList():List<ProductResponse>{
        val product_1 =  ProductResponse("123", "Xiamo", "Smart Phone", BigDecimal(25),10);
        val product_2 =  ProductResponse("8574", "Lorem", "Description lorem", BigDecimal(100),2);
        val product_3 =  ProductResponse("8901", "Lait Nido", "Alimentation", BigDecimal(180), 238);
        val product_4 =  ProductResponse("2345", "Disk dure", "Matériel informatique", BigDecimal(500), 12);
        val product_5 =  ProductResponse("67789", "Gamme de produit Johnson", "Produit d'entretien pour bébé", BigDecimal(300), 100);

        return Arrays.asList(product_1,product_2,product_3,product_4,product_5);

    }

}


